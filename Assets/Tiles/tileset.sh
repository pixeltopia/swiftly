#!/usr/bin/env bash

rm -rf   tileset.atlas
mkdir -p tileset.atlas

convert tileset.png -crop 32x32 tileset.atlas/%03d.png

//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import SpriteKit

class MainScene: BaseScene {
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        let (w, h) = (21, 21)
        
        let model = Grid(area: Area(size: (w, h))) { (cell, inout tile: Tile) in
            let x = cell.x == 0 || cell.x == w - 1
            let y = cell.y == 0 || cell.y == h - 1
            let c = cell.x > 4 && cell.x < w - 5 && cell.y % 4 == 0
            
            if x || y || c {
                tile = Tile(1, solid: true)
            } else {
                tile = Tile(0)
            }
        }
        
        let tilesize = CGSize(width: 32, height: 32)
        let atlas    = SortedTextureAtlas(named: "tileset", filteringMode: .Nearest)
        grid         = GridNode(tilesize: tilesize, model: model, atlas: atlas)
        hero         = SKSpriteNode(texture: atlas[2], color: SKColor.palette[11], size: CGSize(width: 32, height: 32))
        self.atlas   = atlas
        
        if let grid = grid, hero = hero, camera = camera {
            grid.anchorPoint = CGPoint.zero
            self << grid
            
            hero.anchorPoint      = CGPoint.zero
            hero.colorBlendFactor = 1
            self << hero
            
            hero.position   = grid.frame.mid - hero.size.vector / 2
            camera.position = hero.frame.mid
            
            grid.animate(Tile(2), indices: [1, 2], timePerFrame: 0.25)
            grid.animateColorIndex(timePerFrame: 0.5)
        }
    }

    override func didUpdate(currentTme: NSTimeInterval, deltaTime: NSTimeInterval) {
        if let grid = grid, hero = hero, var velocity = velocity {
            let (rect, axis, collisions) = grid.collide(hero.frame, delta: velocity * deltaTime)
            hero.position                = rect.origin
            
            if axis.h || axis.v {
                hero.color = SKColor.palette[3]
                
                if axis.h {
                    velocity.dx *= -1
                }
                
                if axis.v {
                    velocity.dy *= -1
                }
                
                self.velocity = velocity
            } else {
                hero.color = SKColor.palette[11]
            }
            
            for next in collisions {
                let h = next.cell.x == 0 || next.cell.x == grid.model.area.w - 1
                let v = next.cell.y == 0 || next.cell.y == grid.model.area.h - 1
                
                if h || v {
                    grid.model[next.cell.x, next.cell.y] = Tile(2, solid: true)
                } else {
                    grid.model[next.cell.x, next.cell.y] = Tile(0, solid: false)
                }
            }
        }
    }

    override func tap(recognizer: UITapGestureRecognizer) {
        if let hero = hero {
            let delta = convertPointFromView(recognizer.locationInView(view)) - hero.frame.mid
            velocity  = delta.unit * 256
        }
    }

    var atlas: SortedTextureAtlas?
    var grid: GridNode?
    var hero: SKSpriteNode?
    var velocity: CGVector?
}

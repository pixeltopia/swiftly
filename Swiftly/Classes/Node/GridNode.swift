//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import SpriteKit
import CoreGraphics

class GridNode: SKSpriteNode, Viewported {
    init(tilesize: CGSize, model: Grid, atlas: SortedTextureAtlas) {
        self.tilesize   = tilesize
        self.model      = model
        self.atlas      = atlas
        collider        = Collider(tilesize: tilesize, model: model)        
        tints           = [SKColor](SKColor.palette)
        viewing         = Area()
        cached          = [TileNode]()
        animated        = [TileNode]()
        animations      = [Val: SKTexture]()
        version         = model.version
        
        func size(size: CGSize, _ area: Area) -> CGSize {
            return CGSize(width: size.width * CGFloat(area.w), height: size.height * CGFloat(area.h))
        }

        super.init(texture: nil, color: SKColor.clear, size: size(tilesize, model.area))
    }

    required init?(coder decoder: NSCoder) {
        tilesize    = CGSize.zero
        model       = Grid()
        atlas       = SortedTextureAtlas()
        collider    = Collider(tilesize: tilesize, model: model)
        tints       = [SKColor](SKColor.palette)
        viewing     = Area()
        cached      = [TileNode]()
        animated    = [TileNode]()
        animations  = [Val: SKTexture]()
        version     = model.version

        super.init(coder: decoder)
    }
    
    func animate(tile: Tile, indices: [Int], timePerFrame seconds: NSTimeInterval) {
        let textures = atlas.textures(indices)
        let duration = seconds * Double(indices.count)
        let repeated = SKAction.customActionWithDuration(duration) { (node, elapsed) in
            if let node = node as? GridNode {
                let ratio = Double(elapsed) / duration
                let index = Int(ratio * Double(indices.count)) % indices.count
                node.animations[tile.value] = textures[index]
            }
        }
        runAction(SKAction.repeatActionForever(repeated))
    }

    func animateColorIndex(timePerFrame duration: NSTimeInterval) {
        let repeated = SKAction.customActionWithDuration(duration) { (node, elapsed) in
            if let node = node as? GridNode where elapsed =~ CGFloat(duration) {
                node.nextColorIndex = node.colorIndex + 1
            }
        }
        runAction(SKAction.repeatActionForever(repeated))
    }
    
    func update(viewport: CGRect) {
        if let intersected = frame.intersection(viewport) {
            let restricted = model.area.restrict(Area.from(intersected.relativeTo(frame), tilesize: tilesize))
            if viewing != restricted || version != model.version || colorIndex != nextColorIndex {
                viewing    = restricted
                colorIndex = nextColorIndex
                layout()
            }
        }

        animate()
    }

    private func layout() {
        func build() -> TileNode? {
            let result          = TileNode(color: SKColor.clear, size: tilesize)
            result.userData     = [:]
            result.anchorPoint  = CGPoint.zero
            return result
        }
        
        func layout(node: TileNode, cell: Cell, tile: Tile) {
            node.position           = frame.origin + tilesize.vector * CGVector(dx: cell.x, dy: cell.y)
            node.texture            = atlas[tile.value % atlas.count]
            node.color              = tints[(cell.x + cell.y + nextColorIndex) % tints.count]
            node.colorBlendFactor   = 1
            node.tile               = tile
        }

        removeAllChildren()
        
        var nodes = [TileNode]()
        for next in viewing {
            if let node = cached.popLast() ?? build() {
                layout(node, cell: next, tile: model[next.x, next.y])
                nodes << node
            }
        }
        
        cached << nodes
        self   << nodes
        
        animated(nodes)
    }

    private func animated(nodes: [TileNode]) {
        animated = nodes.filter { node in
            if let tile = node.tile {
                return self.animations[tile.value] != nil
            } else {
                return false
            }
        }
    }
    
    private func animate() {
        for next in animated {
            if let tile = next.tile {
                next.texture = animations[tile.value]
            }
        }
    }
    
    func collide(rect: CGRect, delta: CGVector) -> ColliderResult {
        var result  = collider.collide(rect.relativeTo(frame), delta: delta)
        result.rect = result.rect + frame.origin.vector
        return result
    }

    let collider: Collider
    let tilesize: CGSize
    let model: Grid
    let atlas: SortedTextureAtlas
    let tints: [SKColor]

    var viewing: Area
    var cached, animated: [TileNode]
    var animations: [Val: SKTexture]
    var version: Grid.Version
    
    var colorIndex: Int = 0
    var nextColorIndex: Int = 0
}

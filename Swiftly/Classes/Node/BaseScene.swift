//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import SpriteKit

class BaseScene: SKScene {
    override func didMoveToView(view: SKView) {
        func camera() {
            let camera  = SKCameraNode()
            self.camera = camera
            self << camera
        }
        
        func action() {
            var types = [Selector: UIGestureRecognizer.Type]()
            
            types["pan:"]   = UIPanGestureRecognizer.self
            types["tap:"]   = UITapGestureRecognizer.self
            types["pinch:"] = UIPinchGestureRecognizer.self
            
            for (selector, type) in types {
                view.addGestureRecognizer(type.init(target: self, action: selector))
            }
        }
        
        camera()
        action()
    }
    
    override func addChild(node: SKNode) {
        if let node = node as? Viewported {
            viewported << node
        }
        super.addChild(node)
    }
    
    override func update(currentTime: NSTimeInterval) {
        if previous == nil {
            previous = currentTime
        } else {
            didUpdate(currentTime, deltaTime: currentTime - previous!)
        }
        previous = currentTime
    }

    func didUpdate(currentTme: NSTimeInterval, deltaTime: NSTimeInterval) {
    }
    
    override func didFinishUpdate() {
        if let view = view {
            let viewport = convertRectFromView(view.bounds)
            for next in viewported {
                next.update(viewport)
            }
        }
    }

    func pan(recognizer: UIPanGestureRecognizer) {
        if let camera = camera {
            camera.position -= recognizer.translationInView(view).vector * CGVector(dx: 1, dy: -1) * camera.scale
        }
        recognizer.setTranslation(CGPoint.zero, inView: view)
    }
    
    func tap(recognizer: UITapGestureRecognizer) {
        if let camera = camera {
            let action        = SKAction.moveTo(convertPointFromView(recognizer.locationInView(view)), duration: 0.5)
            action.timingMode = .EaseInEaseOut

            camera.runAction(action)
        }
    }
    
    func pinch(recognizer: UIPinchGestureRecognizer) {
        if let camera = camera {
            camera.setScale(camera.xScale * recognizer.scaling)
        }
    }
    
    var viewported = [Viewported]()
    var previous: NSTimeInterval?
}

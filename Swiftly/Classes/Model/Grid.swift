//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

class Grid {
    typealias Load    = (cell: Cell, inout tile: Tile) -> ()
    typealias Version = UInt64

    init(area: Area = Area(), load: Load? = nil) {
        self.area = area
        grid      = [Tile](count: area.count, repeatedValue: Tile(0))
        version   = 0
        
        for next in area {
            load?(cell: next, tile: &self[next.x, next.y])
        }
    }
    
    subscript(x: Int, y: Int) -> Tile {
        get {
            return grid[index(x, y)]
        }
        set {
            grid[index(x, y)] = newValue
            version += 1
        }
    }
    
    private func index(x: Int, _ y: Int) -> Int {
        return y * area.w + x
    }
    
    let area: Area
    var grid: [Tile]
    var version: Version
}

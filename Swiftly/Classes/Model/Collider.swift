//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import CoreGraphics

typealias ColliderResult = (rect: CGRect, axis: (h: Bool, v: Bool), collisions: [Collision])
typealias Collision      = (cell: Cell, tile: Tile)

class Collider {
    init(tilesize: CGSize, model: Grid) {
        self.tilesize = tilesize
        self.model    = model
    }
    
    func collide(rect: CGRect, delta: CGVector) -> ColliderResult {
        var result      = ColliderResult(CGRect.null, (false, false), [])
        var working     = rect
        let horizontal  = CGVector(dx: delta.dx, dy: 0)
        let vertical    = CGVector(dx: 0, dy: delta.dy)
        let inset       = CGFloat(0.1)
        var collisions  = [Collision]()
        
        func xtile(x: Int) -> CGFloat { return CGFloat(x) * tilesize.width  }
        func ytile(y: Int) -> CGFloat { return CGFloat(y) * tilesize.height }
        
        if let collision = collision(working, delta: horizontal) {
            collisions << collision
            if horizontal.dx > 0 {
                working.origin = CGPoint(x: xtile(collision.cell.x + 0) - working.width - inset, y: working.origin.y)
            }
            if horizontal.dx < 0 {
                working.origin = CGPoint(x: xtile(collision.cell.x + 1) + inset, y: working.origin.y)
            }
            result.axis.h = true
        } else {
            working.origin += horizontal
        }
        
        if let collision = collision(working, delta: vertical) {
            collisions << collision
            if vertical.dy > 0 {
                working.origin = CGPoint(x: working.origin.x, y: ytile(collision.cell.y + 0) - working.height - inset)
            }
            if vertical.dy < 0 {
                working.origin = CGPoint(x: working.origin.x, y: ytile(collision.cell.y + 1) + inset)
            }
            result.axis.v = true
        } else {
            working.origin += vertical
        }
        
        result.collisions = collisions
        result.rect       = working
        
        return result
    }
    
    private func collision(rect: CGRect, delta: CGVector) -> Collision? {
        for next in model.area.restrict(Area.from(rect.union(rect + delta), tilesize: tilesize)) {
            if model[next.x, next.y].solid {
                return (cell: next, tile: model[next.x, next.y])
            }
        }
        return nil
    }

    let tilesize: CGSize
    let model: Grid
}

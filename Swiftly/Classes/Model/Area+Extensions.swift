//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import CoreGraphics

extension Area {
    static func from(rect: CGRect, tilesize: CGSize) -> Area {
        let scaled = rect / tilesize.vector
        
        func cell(point: CGPoint) -> Cell {
            return Cell(x: Int(floor(point.x)), y: Int(floor(point.y)))
        }
        
        return Area(min: cell(scaled.min), max: cell(scaled.max))
    }
}

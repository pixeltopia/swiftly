//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

public typealias Cell = (x: Int, y: Int)

public struct Area: SequenceType, Equatable {
    init(min: Cell, max: Cell) {
        self.min = min
        self.max = max
    }
    
    init(size: (w: Int, h: Int)) {
        self.init(min: (0, 0), max: (size.w - 1, size.h - 1))
    }
    
    init() {
        self.init(size: (0, 0))
    }

    public func generate() -> AnyGenerator<Cell> {
        var sequence = [Cell]()
        var next     = 0
        
        for y in min.y...max.y {
            for x in min.x...max.x {
                sequence << (x, y)
            }
        }
        
        return anyGenerator {
            defer {
                next += 1
            }
            guard next < sequence.count else {
                return nil
            }
            return sequence[next]
        }
    }

    func restrict(other: Area) -> Area {
        let maximum = (Swift.max(min.x, other.min.x), Swift.max(min.y, other.min.y))
        let minimum = (Swift.min(max.x, other.max.x), Swift.min(max.y, other.max.y))

        return Area(min: maximum, max: minimum)
    }
    
    var w: Int {
        return max.x - min.x + 1
    }
    
    var h: Int {
        return max.y - min.y + 1
    }
    
    var count: Int {
        return w * h
    }

    let min, max: Cell
}

public func == (lhs: Area, rhs: Area) -> Bool {
    return lhs.min == rhs.min && lhs.max == rhs.max
}

public func == (lhs: Cell, rhs: Cell) -> Bool {
    return lhs.x == rhs.x && lhs.y == rhs.y
}

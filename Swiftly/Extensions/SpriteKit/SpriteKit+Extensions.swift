//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import SpriteKit

extension SKCameraNode {
    var scale: CGVector {
        return CGVector(dx: xScale, dy: yScale)
    }
}

extension SKScene {
    func convertRectFromView(rect: CGRect) -> CGRect {
        let min = convertPointFromView(rect.min)
        let max = convertPointFromView(rect.max)
        
        return CGRect(origin: min, size: (max - min).size).standardized
    }
}

extension SKColor {
    convenience init(_ hex: UInt) {
        let a = CGFloat((hex & 0xFF000000) >> (8 * 3)) / 255.0
        let r = CGFloat((hex & 0x00FF0000) >> (8 * 2)) / 255.0
        let g = CGFloat((hex & 0x0000FF00) >> (8 * 1)) / 255.0
        let b = CGFloat((hex & 0x000000FF) >> (8 * 0)) / 255.0
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }
}

extension SKColor {
    static var palette: [SKColor] {
        var result = [SKColor]()
        
        result << SKColor(0xFF000000)
        result << SKColor(0xFF9D9D9D)
        result << SKColor(0xFFFFFFFF)
        result << SKColor(0xFFBE2633)
        result << SKColor(0xFFE06F8B)
        result << SKColor(0xFF493C2B)
        result << SKColor(0xFFA46422)
        result << SKColor(0xFFEB8931)
        result << SKColor(0xFFF7E26B)
        result << SKColor(0xFF2F484E)
        result << SKColor(0xFF44891A)
        result << SKColor(0xFFA3CE27)
        result << SKColor(0xFF1B2632)
        result << SKColor(0xFF005784)
        result << SKColor(0xFF31A2F2)
        result << SKColor(0xFFB2DCEF)
        
        return result
    }
}

extension SKColor {
    static var clear: SKColor {
        return clearColor()
    }
}

class SortedTextureAtlas  {
    init(named name: String, filteringMode: SKTextureFilteringMode) {
        atlas              = SKTextureAtlas(named: name)
        sortedTextureNames = atlas.textureNames.sort()

        for next in sortedTextureNames {
            atlas.textureNamed(next).filteringMode = filteringMode
        }
    }
    
    convenience init() {
        self.init(named: "", filteringMode: .Linear)
    }
    
    func textures(indices: [Int]) -> [SKTexture] {
        return indices.map { next in
            atlas.textureNamed(sortedTextureNames[next])
        }
    }
    
    var count: Int {
        return sortedTextureNames.count
    }
    
    subscript(index: Int) -> SKTexture {
        return atlas.textureNamed(sortedTextureNames[index])
    }
    
    let sortedTextureNames: [String]
    let atlas: SKTextureAtlas
}

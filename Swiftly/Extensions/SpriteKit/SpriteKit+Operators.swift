//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import SpriteKit

func << (lhs: SKNode, rhs: [SKNode]) {
    for next in rhs {
        lhs << next
    }
}

func << (lhs: SKNode, rhs: SKNode) {
    guard rhs.parent == nil else {
        return
    }
    lhs.addChild(rhs)
}

func << (lhs: SKNode, rhs: SKNode?) {
    guard let rhs = rhs else {
        return
    }
    lhs << rhs
}

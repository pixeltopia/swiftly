//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import CoreGraphics

extension CGPoint {
    var vector: CGVector {
        return CGVector(dx: x, dy: y)
    }
}

extension CGSize {
    var vector: CGVector {
        return CGVector(dx: width, dy: height)
    }
}

extension CGRect {
    var min: CGPoint {
        get {
            return CGPoint(x: minX, y: minY)
        }
        set {
            origin = newValue
        }
    }
    
    var mid: CGPoint {
        get {
            return CGPoint(x: midX, y: midY)
        }
        set {
            origin = newValue - size.vector / CGVector(dx: 2, dy: 2)
        }
    }

    var max: CGPoint {
        get {
            return CGPoint(x: maxX, y: maxY)
        }
        set {
            size = (newValue - min).size
        }
    }
    
    func intersection(other: CGRect) -> CGRect? {
        let rect = intersect(other)
        guard rect != CGRect.null else {
            return nil
        }
        return rect
    }
    
    func relativeTo(other: CGRect) -> CGRect {
        return CGRect(origin: (origin - other.origin).point, size: size)
    }
}

extension CGVector {
    var point: CGPoint {
        return CGPoint(x: dx, y: dy)
    }
    
    var size: CGSize {
        return CGSize(width: dx, height: dy)
    }
    
    var integral: CGVector {
        return CGVector(dx: round(dx), dy: round(dy))
    }

    var magnitude: Double {
        return sqrt(Double(dx * dx + dy * dy))
    }
    
    var unit: CGVector {
        return CGVector(dx: Double(dx) / magnitude, dy: Double(dy) / magnitude)
    }

    struct Polar {
        var angle, distance: Double
        var vector: CGVector {
            return CGVector(dx: distance * cos(angle), dy: distance * sin(angle))
        }
    }
    
    var polar: Polar {
        return Polar(angle: atan2(Double(dy), Double(dx)), distance: magnitude)
    }
    
    func angleBetween(other: CGVector) -> Double {
        var result = other.polar.angle - polar.angle
        if (result < 0) {
            result += 2 * M_PI
        }
        return result
    }
}

extension Double {
    var radians: Double {
        return self * M_PI / 180.0
    }

    var degrees: Double {
        return self * 180.0 / M_PI
    }
}

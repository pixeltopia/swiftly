//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import CoreGraphics

//
// <Approximate> [~EQ] <Approximate> := <Bool>
//

infix operator =~ { precedence 130 }
infix operator !~ { precedence 130 }

protocol Approx {
    func =~ (lhs: Self, rhs: Self) -> Bool
    func !~ (lhs: Self, rhs: Self) -> Bool
}

extension CGVector: Approx {}
extension CGRect: Approx {}
extension CGSize: Approx {}
extension CGPoint: Approx {}
extension CGFloat: Approx {}

func !~ <T: Approx> (lhs: T, rhs: T) -> Bool {
    return !(lhs =~ rhs)
}

func =~ (lhs: CGVector, rhs: CGVector) -> Bool {
    return lhs.dx =~ rhs.dx && lhs.dy =~ rhs.dy
}

func =~ (lhs: CGRect, rhs: CGRect) -> Bool {
    return lhs.origin =~ rhs.origin && lhs.size =~ rhs.size
}

func =~ (lhs: CGSize, rhs: CGSize) -> Bool {
    return lhs.width =~ rhs.width && lhs.height =~ rhs.height
}

func =~ (lhs: CGPoint, rhs: CGPoint) -> Bool {
    return lhs.x =~ rhs.x && lhs.y =~ rhs.y
}

func =~ (lhs: CGFloat, rhs: CGFloat) -> Bool {
    return Double(fabs(lhs - rhs)) <= EPSILON
}

private let EPSILON = 0.0001

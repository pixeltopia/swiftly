//
// Copyright 2015 David Birdsall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import CoreGraphics

//
// <CGPoint> [ADD|SUB|MUL|DIV] <CGVector> := <CGPoint>
//

func += (inout lhs: CGPoint, rhs: CGVector) {
    lhs = lhs + rhs
}

func -= (inout lhs: CGPoint, rhs: CGVector) {
    lhs = lhs - rhs
}

func *= (inout lhs: CGPoint, rhs: CGVector) {
    lhs = lhs * rhs
}

func /= (inout lhs: CGPoint, rhs: CGVector) {
    lhs = lhs / rhs
}

func + (lhs: CGPoint, rhs: CGVector) -> CGPoint {
    return CGPoint(x: lhs.x + rhs.dx, y: lhs.y + rhs.dy)
}

func - (lhs: CGPoint, rhs: CGVector) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.dx, y: lhs.y - rhs.dy)
}

func * (lhs: CGPoint, rhs: CGVector) -> CGPoint {
    return CGPoint(x: lhs.x * rhs.dx, y: lhs.y * rhs.dy)
}

func / (lhs: CGPoint, rhs: CGVector) -> CGPoint {
    return CGPoint(x: lhs.x / rhs.dx, y: lhs.y / rhs.dy)
}

//
// <CGPoint> [SUB] <CGPoint> := <CGVector>
//

func - (lhs: CGPoint, rhs: CGPoint) -> CGVector {
    return CGVector(dx: lhs.x - rhs.x, dy: lhs.y - rhs.y)
}

//
// <CGRect> [ADD|SUB|MUL|DIV] <CGVector> := <CGRect>
//

func += (inout lhs: CGRect, rhs: CGVector) {
    lhs = lhs + rhs
}

func -= (inout lhs: CGRect, rhs: CGVector) {
    lhs = lhs - rhs
}

func *= (inout lhs: CGRect, rhs: CGVector) {
    lhs = lhs * rhs
}

func /= (inout lhs: CGRect, rhs: CGVector) {
    lhs = lhs / rhs
}

func + (lhs: CGRect, rhs: CGVector) -> CGRect {
    return CGRect(origin: lhs.origin + rhs, size: lhs.size)
}

func - (lhs: CGRect, rhs: CGVector) -> CGRect {
    return CGRect(origin: lhs.origin - rhs, size: lhs.size)
}

func * (lhs: CGRect, rhs: CGVector) -> CGRect {
    return CGRect(origin: lhs.origin * rhs, size: (lhs.size.vector * rhs).size)
}

func / (lhs: CGRect, rhs: CGVector) -> CGRect {
    return CGRect(origin: lhs.origin / rhs, size: (lhs.size.vector / rhs).size)
}

//
// <CGVector> [MUL|DIV] <CGVector> := <CGVector>
//

func *= (inout lhs: CGVector, rhs: CGVector) {
    lhs = lhs * rhs
}

func /= (inout lhs: CGVector, rhs: CGVector) {
    lhs = lhs / rhs
}

func * (lhs: CGVector, rhs: CGVector) -> CGVector {
    return CGVector(dx: lhs.dx * rhs.dx, dy: lhs.dy * rhs.dy)
}

func / (lhs: CGVector, rhs: CGVector) -> CGVector {
    return CGVector(dx: lhs.dx / rhs.dx, dy: lhs.dy / rhs.dy)
}

//
// <CGVector> [MUL|DIV] <Double> := <CGVector>
//

func *= (inout lhs: CGVector, rhs: Double) {
    lhs = lhs * rhs
}

func /= (inout lhs: CGVector, rhs: Double) {
    lhs = lhs / rhs
}

func * (lhs: CGVector, rhs: Double) -> CGVector {
    return CGVector(dx: lhs.dx * CGFloat(rhs), dy: lhs.dy * CGFloat(rhs))
}

func / (lhs: CGVector, rhs: Double) -> CGVector {
    return CGVector(dx: lhs.dx / CGFloat(rhs), dy: lhs.dy / CGFloat(rhs))
}

//
// <CGVector> [DOT (Option + 8)] <CGVector> := <Double>
//

infix operator • { associativity left precedence 140 }

func • (lhs: CGVector, rhs: CGVector) -> Double {
    return Double(lhs.dx * rhs.dx + lhs.dy * rhs.dy)
}
